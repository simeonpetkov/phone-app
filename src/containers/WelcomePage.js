import React, { Component } from 'react'

import { Link } from 'react-router-dom';

import logo from '../icons/heading-logo.png';

class WelcomePage extends Component {
  render() {
    return (
      <div className="c-initial-page">
        <div className="c-initial-page--wrapper">
          <img className="c-logo--big" src={logo} alt="c-heading-logo" />
          <h1 className="c-heading">Welcome to PhoneTech</h1>
          <Link to="/dashboard" className="c-btn-heading">
            <button className="c-btn c-btn--main">
              GO TO CATALOGUE
            </button>
          </Link>
        </div>
      </div>
    )
  }
}

export default WelcomePage;
