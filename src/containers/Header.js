import React, { Component } from 'react'

import logo from '../icons/heading-logo.png';

class Header extends Component {
  render() {
    const { route, btnVisible, onOpenModal } = this.props;
    return (
      <div className="c-header">
        <img className="c-logo--small" src={logo} alt="header-logo" /> {/* left */}
        <h1>{route}</h1> {/* middle */}
        {btnVisible &&
        <button onClick={onOpenModal}>Add phone</button>} {/* right */}
      </div>
    )
  }
}

export default Header;
