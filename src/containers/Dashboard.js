import React, { Component } from 'react'
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import Modal from './Modal';
import { addPhones } from '../store/reduxModules/phones';
import _ from 'lodash';

import Header from './Header';
import ListRow from './ListRow';
import Search from './SearchForm';
import '../sass/_dashboard.scss';

class Dashboard extends Component {
  state = {
    phones: this.props.phones,
    modalVisible: false,
  }

componentWillReceiveProps(nextPros) {
  if (this.props.phones !== nextPros.phones) {
    this.setState({ phones: nextPros.phones });
  }
}

  onShowPopup = () => {
    this.setState({ modalVisible: true });
  }

  onHidePopup = () => {
    this.setState({ modalVisible: false });
  }

  onSubmitHandler = (values) => {
    this.props.onAddPhone({
      ...values,
      price: values.price.toString(),
      camera: values.camera.toString(),
    });
    this.onHidePopup();
  }

  onSearchHandler = (values) => {
    const { phones } = this.props;
    const lowerCaseArray = phones.map(p => {
      const modifObj = {};
      for (var key in p)
        if (p.hasOwnProperty(key) && key !== 'id') {
          modifObj[key] = p[key].toLowerCase();
        } else if (key === 'id')
          modifObj[key] = p[key];
      return modifObj;
    })
    const filteredPhones = _.filter(lowerCaseArray, function(p) {
      return _.includes(p, values.search.toLowerCase());
    });
    this.setState({ 
      phones: filteredPhones.map(p => phones.find(phone => phone.id === p.id))
    })
  }

  resetSearch = () => {
    this.setState({ phones: this.props.phones });
  }

  render() {
    const { phones, modalVisible } = this.state;
    return (
      <div>
        <Header 
          route="Dashboard"
          onOpenModal={this.onShowPopup}
          btnVisible
        /> 
        <div style={{ paddingLeft: '30px' }}>
          <h4 className="search-heading">Filter results</h4>    
          <Search 
            onSubmit={this.onSearchHandler}
            onClickSeeAll={this.resetSearch}
          />   
        </div>
        <div className="table-container">
          <table className="c-dashboard-table">
            <tr className="table-head">
              <th>Name</th>
              <th>Model</th>
              <th>Camera</th>
              <th>Color</th>
              <th>Price</th>
            </tr>
            <tbody>
              {phones.map(p => {
                  return (
                      <ListRow phone={p} />
                  )
                })
              }
            </tbody>
          </table>
        </div>

        {modalVisible && 
          <Modal hidePopup={this.onHidePopup} onSubmit={this.onSubmitHandler} />
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    phones: state.firesore.ordered.phones || [],
  }
}

const mapDispatchToProps = dispatch => ({
  onAddPhone: (phoneData) => dispatch(addPhones(phoneData)),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firestoreConnect([
    { 'collection': 'phones' }
  ])
)(Dashboard);
