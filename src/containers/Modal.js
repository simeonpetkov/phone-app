import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form';

import PopupContainer from './PopupContainer';

import closeSVG from '../icons/close-icon.svg';

class Modal extends Component {
  render() {
    const { hidePopup } = this.props;
    return (
      <PopupContainer formClassName="c-popup" hidePopup={() => { hidePopup(); }}>
        <form className="u-center-column" onSubmit={this.props.handleSubmit}>
        <img src={closeSVG} alt="close-svg" className="c-popup__close" onClick={hidePopup} />
        <h2 className="c-popup__title">Add phone</h2>
          <Field
            className="c-form-input c-form-input--secondary"
            name="name"
            type="text"
            component="input"
            placeholder="Name..."
            required
          />
          <Field
            className="c-form-input c-form-input--secondary"
            name="model"
            type="text"
            component="input"
            placeholder="Model..."
            required
          />
          <Field
            className="c-form-input c-form-input--secondary"
            name="camera"
            type="number"
            component="input"
            placeholder="Camera..."
            required
          />
          <Field
            className="c-form-input c-form-input--secondary"
            name="color"
            type="text"
            component="input"
            placeholder="Color..."
            required
          />
          <Field
            className="c-form-input c-form-input--secondary"
            name="price"
            type="number"
            component="input"
            placeholder="Price..."
            required
          />
          <div className="o-btn-wrapper">
            <button
              className="c-btn c-btn--main c-btn--main--popup"
              type="submit"
            >
              ADD
            </button>
          </div>
        </form>
      </PopupContainer>
    )
  }
}

Modal = reduxForm({ // eslint-disable-line
  form: 'addPhone',
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
})(Modal);

export default Modal;