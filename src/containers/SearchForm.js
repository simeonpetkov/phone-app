import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form';

class SearchForm extends Component {
  render() {
    return (
      <div className="c-search-container" onSubmit={this.props.handleSubmit}>
        <form>
          <Field
              className="c-form-input"
              name="search"
              type="text"
              component="input"
              placeholder="Type here..."
              required
            />
          <button
              className="c-btn c-btn--main"
              type="submit"
          >
            SEARCH
          </button>
        </form>
        <button
            className="c-btn c-btn--main"
            onClick={this.props.onClickSeeAll}
        >
          SEE ALL
        </button>
      </div>
    )
  }
}

SearchForm = reduxForm({ // eslint-disable-line
    form: 'search',
    enableReinitialize: true,
    keepDirtyOnReinitialize: true,
  })(SearchForm);

export default SearchForm;
