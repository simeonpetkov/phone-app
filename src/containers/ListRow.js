import React, { Component } from 'react'

class ListRow extends Component {
  render() {
    const { phone } = this.props;
    return (
      <tr key={phone.id}>
        <td>{phone.name}</td>
        <td>{phone.model}</td>
        <td>{`${phone.camera}MP`}</td>
        <td>{phone.color}</td>
        <td>{`$${phone.price}`}</td>
      </tr>
    )
  }
}

export default ListRow;
