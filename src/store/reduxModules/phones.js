import 'firebase/database';

export const ADD_PHONES = 'ADD_PHONES';

export const addPhones = (phoneData) => (dispatch, getState, db) => {
  db.collection('phones').add({
    ...phoneData,
  })
};